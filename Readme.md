# Extract IDs from Youtube by dumping the network requests

Steps (Firefox):

1. Open new tab
2. F12 to open the console/developer tools
3. Switch to the "Network" tab there
4. Open Youtube (e.g. directly open the [subcriptions link](https://www.youtube.com/feed/subscriptions))
5. Scroll down the page/channel/playlist to display more videos, stop when you want.
6. (Not recommended) Optional: "Filter URLs" - "ajax" to only filter for videos NOT PRESENT on the MAIN PAGE (current URL that retrieves additional videos to display is "https://www.youtube.com/browse_ajax")
7. "Save all as HAR" (remember the path/file name)
   * Firefox bug: make sure the `.har` files aren't completely empty or actually contain ALL requests you made. It doesn't seem to reliably "Save **all** as HAR" (Firefox 86)
8. `lua har-extract-youtube.lua <FILE NAME FROM STEP 7>`
   * if you need to output the URLs, add `> my-list.txt` to the end
9. `youtube-dl --batch-file "my-list.txt"

## Usage:

`lua har-extract-youtube.lua FILE1 [FILE2 ...]`

Outputs IDs in the following format: `https://youtu.be/VIDEO_ID`

The videos are sorted, in the order of discovery within the network request dump.

## Installation / requirements:

Lua.

For Debian/Ubuntu: `sudo apt-get install lua`
