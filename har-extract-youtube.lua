#!/usr/bin/env lua
local args = args or {...}
assert(args[1], "You must provide a HAR file as first argument!")

-- tbl: a [key] = [number] table
-- returns: array of sorted keys
function sortKeysByValues(tbl)
	local sorted = {}
	local compare = function(a,b) return tbl[a] < tbl[b] end
	
	for k, v in pairs(tbl) do
		sorted[#sorted+1] = k
	end
	table.sort(sorted, compare)
	return sorted
end

function extractIds(txt)
	local idList = {}
	local count = 0
	-- /feed/subscriptions: "videoId":"zEhdL4msL98"
	-- JSON, browse_ajax: \"videoId\":\"zEhdL4msL98\"
	for id in txt:gmatch('\\?"[vV]ideoId\\?": *\\?"([%w_%-]+)') do 
		if #id ~= 11 then error("Matched an invalid ID: ".. tostring(id)) end
		
		-- only add/count ID once
		if not idList[id] then
			count = count + 1
			idList[id] = count
		end
		
	end
	return sortKeysByValues(idList)
end

for i = 1, #args do
	local file, err = io.open(args[i], "rb")
	if not file then error(err) end
	local textAll = file:read("*a")
	local idList = extractIds(textAll)
	for i = 1, #idList do
		local id = idList[i]
		print(string.format("https://youtu.be/%s", id))
	end
end